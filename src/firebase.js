import firebase from "firebase/app"
import "firebase/auth"
import "firebase/database"

const app = firebase.initializeApp({
    apiKey: "AIzaSyBBsHiu1RcDIAtenMKf93Dlc_FLLpdmrM0",
    authDomain: "cartelera-acea4.firebaseapp.com",
    databaseURL: "https://cartelera-acea4.firebaseio.com",
    projectId: "cartelera-acea4",
    storageBucket: "cartelera-acea4.appspot.com",
    messagingSenderId: "946081306151",
    appId: "1:946081306151:web:f701ea30b02dee664a2002"
})

export default app;
export const auth = firebase.auth();
export const database = firebase.database();
export const auth1=firebase.auth;
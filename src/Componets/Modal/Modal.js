import React from 'react'
import ReactDom from 'react-dom'
import '../Styles/Modal.css'

function Modal (props){

    if(!props.isOpen){
        return null;
    }

return ReactDom.createPortal(
    <div className="Modal1">
        <div className="Modal_container1">
            <button type="button" onClick={props.onClose}  className="Modal_close-button1">
                Cerrar
            </button>
            <div className="tittle1">
            Reservar Para Esta Pelicula...
            </div>
            <div>
            {props.children}
            </div>
        </div>
    </div>,
    document.getElementById('Modal')
);
}

export default Modal;
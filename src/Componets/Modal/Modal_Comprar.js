import React from 'react'
import ReactDom from 'react-dom'
import '../Styles/ModalComprar.css'

function Modal (props){

    if(!props.isOpen){
        return null;
    }

return ReactDom.createPortal(
    <div className="Modal7">
        <div className="Modal_container7">
            <div className="tittle7">
             Comprar Boleto(s)
            </div>
            <div>
            {props.children}
            </div>
            
        </div>
    </div>,
    document.getElementById('Modal')
);
}

export default Modal;
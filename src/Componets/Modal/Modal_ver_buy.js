import React from 'react'
import ReactDom from 'react-dom'
import '../Styles/Modal_buy_ver.css'

function Modal (props){

    if(!props.isOpen){
        return null;
    }

return ReactDom.createPortal(
    <div className="Modal144">
        <div className="Modal_container144">
            <button type="button" onClick={props.onClose}  className="Modal_close-button144">
                Cerrar
            </button>
            <div className="tittle144">
             Informacion de boleto...
            </div>
            <div>
            {props.children}
            </div>
        </div>
    </div>,
    document.getElementById('Modal')
);
}

export default Modal;
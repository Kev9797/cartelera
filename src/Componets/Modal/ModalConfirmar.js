import React from 'react'
import ReactDom from 'react-dom'
import '../Styles/ModalConfirmar.css'

function Modal (props){

    if(!props.isOpen){
        return null;
    }

return ReactDom.createPortal(
    <div className="Modal22">
        <div className="Modal_container22">
            <div className="tittle22">
             COMPRA REALIZADA CON EXITO
            </div>
            <div>
            {props.children}
            </div>
        </div>
    </div>,
    document.getElementById('Modal')
);
}

export default Modal;
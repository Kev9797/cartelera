import React, {useState, Fragment } from 'react'
import { database } from "../../firebase";
import Target from './Target'
import '../Styles/ver2.css'
import { Link } from 'react-router-dom';

const Frecuentes = () =>{
    const [Boletos, setBoletos] = useState({});
    
    database.ref("/Boletos").on("value", snapshot => {
        const val = snapshot.val();
        if (Object.keys(val).length !== Object.keys(Boletos).length) {
          setBoletos(val);
        }
      });

    return(
        <Fragment>
            <div className="navigator-bar77"> 
               <Link to='/'> <div type="button" className="title77">Cartelera</div> </Link>
               <Link to='/Compras'><div type="button" className="title277">Compras Frecuentes</div></Link>
            </div>
            <div className="paris77">
                <div className="todos77">

                {Object.keys(Boletos).map((key, index) => {
                        return (
                            <div className="contenedor17">
                                <Target
                                    key={index}
                                    Boletos={Boletos[key]}
                                    
                                />
                            </div>  
                            );
                        
                        })}
                    
                </div>
            </div>
        </Fragment>
    )
}

export default Frecuentes;
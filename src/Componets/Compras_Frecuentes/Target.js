import React, {useState} from 'react'
import './tar.css'
import Modal_ver_buy from '../Modal/Modal_ver_buy'


const Target = (props) =>{
    const [state, setState] = useState({
        ModalIsOpen: false,
    });

    const HandleOpenModal = ()=>{
        setState({ 
            ...state,
            ModalIsOpen: true,
            
        });
    }

    const HandleCloseModal = ()=>{
        setState({ 
            ...state,
            ModalIsOpen: false,
            
        });
    }

    return(
        <div className="Target-Container">
            <div className="tiitle-p">{props.Boletos.pelicula}</div>
            <div>Fecha Funcion: {props.Boletos.fecha}</div>
            <div>Fecha Compra: {props.Boletos.fecha_compra}</div>
            <div>Boletos Vendidos: {props.Boletos.boletos}</div>
            <button onClick={HandleOpenModal} className="btn-informacion">Ver mas...</button>
            <Modal_ver_buy
                isOpen={state.ModalIsOpen}
                onClose={HandleCloseModal}
            >
                <img  src={props.Boletos.foto} width="100%" height="300px" />
                <div>Cliente: {props.Boletos.cliente}</div>
                <div>Duracion: {props.Boletos.duracion}</div>
                <div>Tipo Funcion: {props.Boletos.calidad}</div>
                <div>Genero: {props.Boletos.genero}</div>
                <div>Sala: {props.Boletos.sala}</div>
                <div>Idioma: {props.Boletos.idioma}</div>
            </Modal_ver_buy>   
        </div>
    )
}

export default Target;
import React, {useState }  from 'react'
import './Cinema.css'

const Componente1 = (props) =>{
   
    return(
        <div className="conteiner-accent" id={props.llave} >
            <div className="accent-n">
                {props.aciento}
            </div>
            <button disabled={props.values} onClick={props.handleboton}  className="btn-seleccion">S</button>
            <button disabled={props.botonc} onClick={props.handleCboton} className="btn-cancelar">C</button>
        </div>
    )

}

export default Componente1;
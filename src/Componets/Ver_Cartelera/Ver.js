import React, {useState, Fragment} from 'react'
import Card from './Card'
import { database } from "../../firebase";
import '../Styles/ver.css'
import 'date-fns';
import Grid from '@material-ui/core/Grid';
import DateFnsUtils from '@date-io/date-fns';
import {
  MuiPickersUtilsProvider,
  KeyboardDatePicker,
} from '@material-ui/pickers';
import { Link } from 'react-router-dom';

const Ver = () => {
    const [Peliculas, setPeliculas] = useState({});
    var f = new Date();
    var [selectedDate, setSelectedDate] = React.useState(new Date(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate()) );
    var fechaselect;
    var Peliculastotal = [];
    const handleDateChange = date => {
      setSelectedDate(date);
    };

    database.ref("/Peliculas").on("value", snapshot => {
        const val = snapshot.val();
        if (Object.keys(val).length !== Object.keys(Peliculas).length) {
          setPeliculas(val);
        }
      });

      const handleformato = () => {
        var nuevo;
       
        var arraydate = selectedDate + "";
        var nuevo = arraydate.split(" ");
    
           fechaselect = nuevo[2];
          switch(nuevo[1]){
            case 'Jan':  fechaselect += "/01/";
              break;
            case 'Feb':  fechaselect += "/02/";
                break;
            case 'Mar':  fechaselect += "/03/";
                break;
            case 'Apr':  fechaselect += "/04/";
                break;
            case 'May':  fechaselect += "/05/";
                break;
            case 'Jun':  fechaselect += "/06/";
                break;
            case 'Jul':  fechaselect += "/07/";
                break;
            case 'Aug':  fechaselect += "/08/";
                break;  
            case 'Sep':  fechaselect += "/09/";
                break;
            case 'Oct':  fechaselect += "/10/";
                break;
            case 'Nov':  fechaselect += "/11/";
                break;
            case 'Dec':  fechaselect += "/12/";
                break;        
          }
          fechaselect += nuevo[3];
    
          
          
          Object.keys(Peliculas).map((key) =>{
              if(Peliculas[key].fecha === fechaselect){
                  Peliculastotal.push(Peliculas[key]);
                  Peliculastotal.push(key);
              }
          })
        
      }


    return(
        <div className="all">
        <div className="navigator-bar"> 
       <Link to='/'><div type="button" className="title">Cartelera</div></Link>
        <div className="Piker-time" >
                         <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <Grid container justify="space-around">
                                <KeyboardDatePicker
                                disableToolbar
                                variant="inline"
                                format="dd/MM/yyyy"
                                margin="normal"
                                id="date-picker-inline"
                                label="Seleccione una Fecha"
                                value={selectedDate}
                                onChange={handleDateChange}
                                KeyboardButtonProps={{
                                    'aria-label': 'change date',
                                }}
                                />
                                {
                                handleformato()
                                }
                            </Grid>
                        </MuiPickersUtilsProvider>
                </div>
           <Link to='/Compras'><div type="button" className="title2">Compras Frecuentes</div></Link> 
        </div>
        <div className="paris">
           
            <div className="todos">
            
            {Object.keys(Peliculastotal).map((key, index) => {

                if(key % 2 == 0){
                    return (
                        
                            
                                <div key={index} className="contenedor1">
                                        <Card
                                            movie={Peliculastotal[key]} 
                                            value={index}
                                            key={index}
                                            llave={key}
                                            kept={Peliculastotal}
                                        />
                                        
                                
                                </div>
                        
                        );
                    }
                    })}
                   
            </div>
                
        </div>
        </div>
    )

}

export default Ver;
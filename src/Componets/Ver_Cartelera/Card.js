import React, {useState, Fragment} from 'react'
import '../Styles/Card_Pelicula.css'
import TextField from "@material-ui/core/TextField";
import Modal from '../Modal/Modal'
import Modal_Comprar from '../Modal/Modal_Comprar'
import Accent from '../Cinema/Accent'
import { database } from "../../firebase";
import jsPDF from 'jspdf';
import Modal_Confirmar from '../Modal/ModalConfirmar'

const Card_Pelcula = (props) =>{
    const [Salas, setSalas] = useState({});
    var f = new Date();
    var fechaselect;
    var [selectedDate] = React.useState(new Date(f.getFullYear() + "-" + (f.getMonth() +1) + "-" + f.getDate()) );
    const [SalaSelect, setSalaSelect] = useState({});
    const [Key, setKey] = useState({});
    const [state, setState] = useState({
        ModalIsOpen: false,
        BotonIsOpen: false,
        BotonCIsOpen: true,
        ModalComprarIsOpen: false,
        ModalConfirmarIsOpen: false,
        numero: {
            value: 1,
            required: true,
            error: false,
            label: 'Cantidad de Boletos:'
        },
        cliente: {
            value: '',
            required: true,
            error: false,
            label: 'Ingrese nombre :'
        },
        codigo: {
            error: false,
        },
    })
    var Disponible = "";
    var VALUES;
    var PRECIO;
    const [value, setvalue] = useState({
        Cantidad: state.numero.value ,
    });
    
    database.ref("/Salas").on("value", snapshot => {
        const val = snapshot.val();
        if (Object.keys(val).length !== Object.keys(Salas).length) {
          setSalas(val);
        }
      });


      const handleformato = () => {
        var nuevo;
       
        var arraydate = selectedDate + "";
        var nuevo = arraydate.split(" ");
    
           fechaselect = nuevo[2];
          switch(nuevo[1]){
            case 'Jan':  fechaselect += "/01/";
              break;
            case 'Feb':  fechaselect += "/02/";
                break;
            case 'Mar':  fechaselect += "/03/";
                break;
            case 'Apr':  fechaselect += "/04/";
                break;
            case 'May':  fechaselect += "/05/";
                break;
            case 'Jun':  fechaselect += "/06/";
                break;
            case 'Jul':  fechaselect += "/07/";
                break;
            case 'Aug':  fechaselect += "/08/";
                break;  
            case 'Sep':  fechaselect += "/09/";
                break;
            case 'Oct':  fechaselect += "/10/";
                break;
            case 'Nov':  fechaselect += "/11/";
                break;
            case 'Dec':  fechaselect += "/12/";
                break;        
          }
          fechaselect += nuevo[3];        
      }  

    const handleOpenModal = e =>{
        var Seleccion = [];
        var Key = [];
        Object.keys(Salas).map((key2, index) =>{
            if(key2 === 'Sala '+props.movie.sala){  
                Object.keys(Salas[key2]).map((key3, index) =>{
                    if(Salas[key2][key3].status === 'Disponible'){
                        Seleccion.push(Salas[key2][key3]);
                        Key.push(key3)
                        setSalaSelect(Seleccion);
                        setKey(Key);
                    }  
                })
                     
            }
        })
        
        setState({ 
            ...state,
            ModalIsOpen: true,
            
        });

        

    }

    const handleOpenModalComprar = e =>{
        
        if(props.movie.disponibilidad > 0){
            if(props.movie.disponibilidad >= state.numero.value){
            setState({ 
                ...state,
                ModalComprarIsOpen: true,
                
            });

            setvalue({ 
                ...value,
                Cantidad: state.numero.value,
                
            }); 
            }
        }

        

    }

    const handleOpenModalConfirmar = e =>{
        setState({ 
            ...state,
            ModalConfirmarIsOpen: true,
            
        });
    }


    const checkRegExp = (obj, regExp, label, labelDefault, size) => {
        if (obj.value == null) {
            obj.value = '';
        }
        var longitud;
        var expresion;
        
        obj.error = (obj.value == size);
        longitud = obj.error;
        obj.error = !(regExp.test(obj.value));
        expresion = obj.error;
        if (longitud) {
            obj.label = `La cantidad minima es de 1 boleto`;
            obj.error = true;
        } else if (expresion) {
            obj.label = label;
        } else {
            obj.label = labelDefault;
        }
        if(props.movie.disponibilidad >= obj.value){
            
            
        }else{
            longitud = obj.error = true;
            
            obj.label = `La cantidad maxima de boletos es ${props.movie.disponibilidad} boleto(s)`;
        }

        return obj;
     }

     const checkRegExpn = (obj22, regExp, label, labelDefault, size) => {
        if (obj22.value == null) {
            obj22.value = '';
        }
        var longitud;
        var expresion;
  
        obj22.error = (obj22.value.length > size);
        longitud = obj22.error;
        obj22.error = !(regExp.test(obj22.value));
        expresion = obj22.error;
        if (longitud) {
            obj22.label = `La longitud maxima es de ${size} caracteres`;
        } else if (expresion) {
            obj22.label = label;
        } else {
            obj22.label = labelDefault;
        }
  
        return obj22;
     }


    const  handleChange = (e) => {
        let obj = null;
        obj = state.numero;
        const { name, value } = e.target;
        switch (name) {
            
            case 'Cantidad':
              
              obj.value = value; 
              obj = checkRegExp(obj,/^([0-9])*$/,'Cantidad no valida','Cantidad',0);
              setState( {...state, numero: obj})
            break;

        }
    }


    const  handleChange22 = (e) => {
        let obj22 = null;
        obj22 = state.cliente;
        const { name, value } = e.target;
        switch (name) {
            
            case 'cliente':
              obj22.value = value; 
              obj22 = checkRegExpn(obj22,/^[a-zA-Z ]*$/,'Nombre no valido','Nombre',22);
              setState( {...state, cliente: obj22})
            break;

        }
    }


    const check = () =>{
        if(props.movie.disponibilidad > 0){
            Disponible = "BOLETOS DISPONIBLES ";
            VALUES = true;
        }else{
             Disponible =  "BOLETOS AGOTADOS CANT. ";
             VALUES =  false;
        }

        PRECIO = state.numero.value * props.movie.precio;
        if(state.cliente.value == ''){
            state.cliente.error = true;
        }else{
            state.cliente.error = false;
        }

        
        if(value.Cantidad == 0){
            state.codigo.error = false;
        }else{
            state.codigo.error = true;
        }
    }

    const handleCloseModal = e =>{
        setState({
            ...state,
            ModalIsOpen: false,
        }) 
    }

    const handleCloseEverything = e =>{
        setState({
            ...state,
            ModalConfirmarIsOpen: false,
            ModalComprarIsOpen: false,
            ModalIsOpen: false,
        }) 
    }

    const handleCloseModalComprar = e =>{
        setState({
            ...state,
            ModalComprarIsOpen: false,
            BotonIsOpen: false,
        }) 

        setvalue({ 
            ...value,
            Cantidad: 1,
            
        });
    }    
    return(
       
        <Fragment>
        <div  type="button" onClick={handleOpenModal} className="card">
            <img  src={props.movie.foto} width="100%" height="300px" />
        </div>
            <Modal 
                isOpen={state.ModalIsOpen}
                onClose={handleCloseModal}> 
                 <Fragment>
                     {check()}
                  <div className="titulo_pel">
                     {props.movie.nombre}
                  </div>
                  <div className="Imagesinfo">
                    <div className="solid">
                    </div>
                    <div className="Informacion">
                        <p>Tipo de Funcion: {props.movie.calidad}</p>
                        <p>Duracion: {props.movie.duracion}</p>
                        <p>Idioma: {props.movie.idioma}</p>
                        <p>Genero: {props.movie.genero}</p>
                        <p>Sala: {props.movie.sala}</p>
                        <div className="roma2" > Precio: $ {props.movie.precio} </div>
                        <div className="roma" >Disponibilidad: {Disponible}{props.movie.disponibilidad}</div>
                        <div className="roma">Esta Funcion comieza a las: {props.movie.hora_inicio} Hr(s)</div>
                    </div>
                  </div>
                    
                  <div className="Compras">
                     <TextField
                        label={state.numero.label}
                        margin="normal"
                        id="outlined-required"
                        name="Cantidad"
                        className="cont-color"
                        value={state.numero.value}
                        required={state.numero.required}
                        onChange={handleChange}
                     />

                     <button className="btn-comprar" onClick={handleOpenModalComprar}> Comprar Boleto(s) </button>
                     <Modal_Comprar
                     isOpen={state.ModalComprarIsOpen}
                     >
                        <Fragment>
                            <div>
                                Reservar a nombre de:
                            </div>

                            <TextField
                                label={state.cliente.label}
                                margin="normal"
                                name="cliente"
                                className="accent-color"
                                value={state.cliente.value}
                                required={state.cliente.required}
                                onChange={handleChange22}
                            />
                            <div>Seleccione sus Acientos</div>
                            <div className="Accent-todo">
                            {Object.keys(SalaSelect).map((key, index) => {
                                return (
                                       
                                          <Accent 
                                            key={index}
                                            llave={key}
                                            aciento={SalaSelect[key].value}
                                            handleboton={() =>{
                                                var ul = document.getElementById(key);
                                                ul.setAttribute("style","background-color:#470052; list-style:none;");
                                        
                                                setvalue({
                                                    ...value,
                                                    Cantidad: value.Cantidad - 1,
                                                })
                                                

                                                if(value.Cantidad == 1){
                                                    setState({ 
                                                        ...state,
                                                        BotonIsOpen: true,
                                                        BotonCIsOpen: false,
                                                    });
                                                }
                                                var newkey;
                                                Object.keys(Key).map((key3, index) =>{
                                                     if(key3 === key){
                                                        newkey = Key[key3];
                                                     }
                                                })

                                                var referencia = database.ref("/Salas/Sala "+props.movie.sala+"/"+newkey
                                                +"/");
                                                referencia.update({
                                                status: 'Ocupado'
                                                });
                                            

                                            }}
                                            handleCboton={() =>{
                                                var ul = document.getElementById(key);
                                                ul.setAttribute("style","background-color:#800000; list-style:none;");
                                                
                                                setvalue({
                                                    ...value,
                                                    Cantidad: value.Cantidad + 1,
                                                })
                                                
                                                if(value.Cantidad >= 0){
                                                    setState({ 
                                                        ...state,
                                                        BotonIsOpen: false,
                                                        BotonCIsOpen: true,
                                                    });
                                                }
                                                
                                                var newkey;
                                                Object.keys(Key).map((key3, index) =>{
                                                     if(key3 === key){
                                                        newkey = Key[key3];
                                                     }
                                                })

                                                var referencia = database.ref("/Salas/Sala "+props.movie.sala+"/"+newkey
                                                +"/");
                                                referencia.update({
                                                status: 'Disponible'
                                                });
                                            }}
                                            values={state.BotonIsOpen}
                                            botonc={state.BotonCIsOpen}
                                          />
                                            
                                    
                                    );
                                })}
                            </div>
                            
                            <div>S = Seleccionar    C = Cancelar</div>
                             <div>
                                 <div>Cantidad de Boletos: {state.numero.value}</div>
                                 <div>Monto a Pagar: $ {PRECIO}.00</div>
                                 <div>Sala: {props.movie.sala}</div>
                                 <div>Fecha: {props.movie.fecha}</div>
                                 <div>Hora de Funcion: {props.movie.hora_inicio} Hrs</div> 
                                 Le Recomentamos estar 15 minutos antes
                                 de la hora de su funcion
                             </div>
                             <button 
                                type="button"
                                disabled={ state.cliente.error || state.codigo.error}  
                                className="Modal_buy-button7"
                                onClick={() =>{
                                    
                                    handleformato();
                                    var referencia = database.ref('/Boletos');
                                    referencia.push({
                                       boletos: state.numero.value,
                                       calidad: props.movie.calidad,
                                       cliente: state.cliente.value,
                                       duracion: props.movie.duracion,
                                       fecha: props.movie.fecha,
                                       foto: props.movie.foto,
                                       genero: props.movie.genero,
                                       idioma: props.movie.idioma,
                                       pelicula: props.movie.nombre,
                                       sala: props.movie.sala,
                                       fecha_compra: fechaselect,
                                    });
                                    
                                    var kepy = props.kept;
                                    var b = parseInt(props.llave);
                                    
                                    var referencia2 = database.ref("/Peliculas/"+kepy[b + 1]+"/");
                                                referencia2.update({
                                                    disponibilidad: props.movie.disponibilidad - state.numero.value
                                                });

                                   handleOpenModalConfirmar(); 
                                   
                                   var doc = new jsPDF();
                                   doc.text(50, 15, 'DETALLES DE COMPRA');
                                   doc.text(50, 20, 'CINE S.A. DE C.V');
                                   doc.text(50, 25, 'CINE UNIVERSIDAD AV 100 C1');
                           
                                   doc.text(50, 45, 'Cliente: '+state.cliente.value);
                                   doc.text(50, 51, 'SALA: '+props.movie.sala);
                                   doc.text(50, 56, 'Tipo de Funcion: '+props.movie.calidad);
                                   doc.text(50, 61, 'Costo por Boleto: '+props.movie.precio+'.00 Pesos');
                                   doc.text(50, 66, 'Costo total: '+PRECIO+'.00 Pesos');
                                   doc.text(50, 71, 'Cantidad Boletos Comprados: '+state.numero.value);
                                   doc.text(50, 76, 'Duracion de Pelicula: '+props.movie.duracion);
                                   doc.text(50, 81, 'Idioma: '+props.movie.idioma);
                                   doc.text(50, 86, 'Genero: '+props.movie.genero);
                                   doc.text(50, 91, 'Fecha Compra: '+fechaselect);
                           
                                   doc.text(50, 100, 'PELICULA A VER: '+props.movie.nombre);
                                   doc.text(50, 110, 'GRACIAS POR SU PREFERENCIA!! ');
                                   doc.text(50, 120, 'DISFRUTE SU FUNCION ');
                                   doc.text(50, 130, 'COMIENZA A LAS: '+props.movie.hora_inicio+" Horas");
                                   doc.text(50, 135, 'DEL DIA: '+props.movie.fecha);

                                   doc.text(10, 145, '---------------------------------------------------------------------------------------------------');
                                                
                                   doc.save(''+props.movie.nombre+'.pdf'); 
                                                
                                }}
                                >
                                    Comprar
                            </button>
                            <Modal_Confirmar
                                isOpen={state.ModalConfirmarIsOpen}
                            >
                             <button type="button" onClick={handleCloseEverything} className="Modal_close-button22">
                                    Aceptar
                            </button>
                            </Modal_Confirmar>
                            <button type="button" onClick={handleCloseModalComprar} className="Modal_close-button7">
                                    Regresar
                            </button>
                            
                        </Fragment>                         
                     </Modal_Comprar>
                  </div>
                  

                </Fragment>
          </Modal>             
        </Fragment>
    )

}

export default Card_Pelcula; 
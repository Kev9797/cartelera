import React from 'react';
import { BrowserRouter, Route, Switch} from 'react-router-dom'
import Ver from './Componets/Ver_Cartelera/Ver'
import Frecuentes from './Componets/Compras_Frecuentes/Frecuentes'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css'

function App() {
  return (
    <BrowserRouter>
       <Switch>
         <Route  exact path="/"> <Ver/> </Route>
         <Route  exact path="/Compras" > <Frecuentes/> </Route>
       </Switch>
    </BrowserRouter>
  );
}

export default App;
